<?php

namespace MatiasMuller\MethodsStacks;

use BadMethodCallException;
use MatiasMuller\MethodsStacks\MethodsStacks;

trait StackableCall
{
	use MethodsStacks;

    protected static $stackableCallPrefix = '__callfrom';

    protected static function getBadMethodCallExceptionClass() {
        return BadMethodCallException::class;
    }

	public function nextCallMethod()
	{
		return '__STACKABLE_CALL__NEXT_CALL_METHOD__';

	    // No utiliza nextStackMethod para mejorar un poco más la performance
		// return $this->nextStackMethod($prefix);
	}

	public function __call($name, $arguments)
	{
		$prefix = self::$stackableCallPrefix;

		foreach (static::getStackMethods($prefix) as $fn) {
			$result = $this->$fn($name, $arguments);
			if ($result !== $this->nextCallMethod()) {
				return $result;
			}
		}

	    // // No utiliza esta implementación relacionada con nextStackMethod
	    // // para mejorar un poco más la performance
		// $result = $this->walkMethodsStack($prefix, [$name, $arguments]);
		// if (!$this->methodsStackPassed($prefix, $result)) {
		// 	return $result;
		// }

		if (get_parent_class(self::class) !== false) {
			return parent::__call($name, $arguments);
		}

		$errorMsg = "Calling unknown method: ".(static::class)."::$name()";
		$badMethodCallExceptionClass = static::getBadMethodCallExceptionClass();
		throw new $badMethodCallExceptionClass($errorMsg);
	}

}

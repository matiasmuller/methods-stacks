<?php

namespace MatiasMuller\MethodsStacks;

trait MethodsStacks
{
    // Variables de caché de clase
	protected static $_classMethods = null;
    protected static $_stackMethods = [];
    protected static $_nextStackMethodKeys = [];

    public static function getClassMethods()
    {
        if (is_null(static::$_classMethods)) {
            static::$_classMethods = get_class_methods(static::class);
        }
        return static::$_classMethods;
    }

    public static function getStackMethods($stackPrefix)
    {
        // TODO: Separar mas el codigo del cache de la implementacion
        if (!isset(static::$_stackMethods[$stackPrefix])) 
        {
            $pendingStack = preg_grep("/^{$stackPrefix}[A-Z].*$/",
                static::getClassMethods()
            );

            // Resuelve las dependencias
            // TODO: Refactorizar mejor, y a futuro optimizar si es necesario
            // con algun algoritmo de orden topologico
            $result = [];
            if ($pendingStack) {
                do {
                    $previousPendingStack = $pendingStack;
                    foreach ($pendingStack as $i => $method) 
                    {
                        $dependenciesProp = "{$method}Depends";
                        $deps = property_exists(static::class, $dependenciesProp)
                            ? array_map(function($dependency) use ($stackPrefix) {
                                return $stackPrefix . ucfirst($dependency);
                            }, static::$$dependenciesProp)
                            : [];

                        if (!$deps || count($deps) === count(array_intersect($deps, $result))) {
                            $result[] = $method;
                            unset($pendingStack[$i]);
                        }
                    }
                    if ($previousPendingStack === $pendingStack) {
                        $errorMsg = "Infinite loop on stack \"$stackPrefix\" of class "
                            . static::class;
                        throw new \Exception($errorMsg);
                    }
                } while ($pendingStack);
            }

            static::$_stackMethods[$stackPrefix] = $result;
        }
        
        return static::$_stackMethods[$stackPrefix];
    }

    public function nextStackMethod($stackPrefix)
    {
        return static::nextStaticStackMethod($stackPrefix);
    }

    public static function nextStaticStackMethod($stackPrefix)
    {
        return "__METHODS_STACKS__NEXT_{$stackPrefix}_METHOD__";
    }

    public function walkMethodsStack($stackPrefix, $arguments = [])
    {
        $result = null;
        foreach (static::getStackMethods($stackPrefix) as $fn) {
            $result = call_user_func_array([$this, $fn], $arguments);
            if ($result !== $this->nextStackMethod($stackPrefix)) {
                break;
            }
        }
        return $result;
    }

    public static function walkStaticMethodsStack($stackPrefix, $arguments = [])
    {
        $result = null;
        foreach (static::getStackMethods($stackPrefix) as $fn) {
            $result = call_user_func_array([static::class, $fn], $arguments);
            if ($result !== static::nextStaticStackMethod($stackPrefix)) {
                break;
            }
        }
        return $result;
    }

    public function methodsStackPassed($stackPrefix, $result)
    {
        return $result === $this->nextStackMethod($stackPrefix);
    }

}